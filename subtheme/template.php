<?php

/**
 * Implements hook_css_alter().
 */
function subtheme_css_alter(&$css) {
	// Replace the fixed-width.css file when the variable is set to responsive
  if(theme_get_setting('goalgorilla_responsive') == 'goalgorilla-responsive-sidebars') {
  	if($css[drupal_get_path('theme', 'goalgorilla') .'/css/layouts/fixed-width.css']) {
  		$oldcss = $css[drupal_get_path('theme', 'goalgorilla') .'/css/layouts/fixed-width.css'];
  		$oldcss['data'] = drupal_get_path('theme', 'goalgorilla') .'/css/layouts/responsive-sidebars.css';
	  	unset($css[drupal_get_path('theme', 'goalgorilla') .'/css/layouts/fixed-width.css']);
	  	$css[drupal_get_path('theme', 'goalgorilla') .'/css/layouts/responsive-sidebars.css'] = $oldcss;		
  	}
	} 
}

/**
 * Implements template_preprocess_node().
 *
 * Remove 'Add comment' and 'New comments' links.
 */
function subtheme_preprocess_node(&$variables) {
  // Remove 'Add comment' link.
  if (isset($variables['content']['links']['comment']['#links']['comment-add'])) {
    unset($variables['content']['links']['comment']['#links']['comment-add']);
  }

  // Remove 'New comments' link
  if (isset($variables['content']['links']['comment']['#links']['comment-new-comments'])) {
    unset($variables['content']['links']['comment']['#links']['comment-new-comments']);
  }
}
