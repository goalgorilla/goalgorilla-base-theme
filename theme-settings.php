<?php
/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function goalgorilla_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  // Create the form using Forms API: http://api.drupal.org/api/7

  $form['support']['goalgorilla_responsive'] = array(
    '#type'          => 'radios',
    '#title'         => t('Goalgorilla layout setting'),
    '#options'       => array(
      'goalgorilla-responsive-sidebars' => t('Responsive sidebar layout') . ' <small>(layouts/responsive-sidebars.css)</small>',
      'goalgorilla-fixed-width' => t('Fixed width layout') . ' <small>(layouts/fixed-width.css)</small>',
    ),
    '#default_value' => theme_get_setting('goalgorilla_responsive'),
    '#description'   => t("Select which layout file you want to load. Select responsive if you want to load the default responsive css file."),
  );
  // */

  // Remove some of the base theme's settings.
  /* -- Delete this line if you want to turn off this setting.
  unset($form['themedev']['zen_wireframes']); // We don't need to toggle wireframes on this site.
  // */

  // We are editing the $form in place, so we don't need to return anything.
}
